console.log('helo world');


//  an array in programming is simply a list of data

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

let studentNumber =['2020-1923','2020-1924','2020-1925','2020-1926','2020-1927'];

// arrays are used to store multiple value in a single variable
// they are declared using squere brackets ([]) also known "array literals"
/*
	syntax:
	let/const arrayName = [elementA, elementB, elementC...]
*/

// common examples of arrays
let grades = [ 98.5, 94.3, 89.2, 90.1];
let computerBrands =['Acer','Asus', 'lenovo', 'neo']

console.log(grades);
console.log(computerBrands);

let myTasks = [
		'drink html',
		'eat Javascript',
		'inhale css',
		'bake sass'
	];
	console.log(myTasks)

	let tasks=[ 'dring', 'eat', 'love', 'dance', 'code'];
	let capitalCities=['manila', 'bangkok', 'saba', 'tokyo'];

	console.log(tasks);
	console.log(capitalCities);

	// length property
	// it allows us to get and set the total number of item in an array
	console.log(tasks.length);
	console.log(capitalCities.length);

	myTasks.length =myTasks.length-2;
	console.log(myTasks)


	console.log(capitalCities)
//  adding in the arry

	let theTrainer = [ 'ash']
	function addTrainer( trainer ){
	theTrainer[theTrainer.length]=trainer;

	}

	addTrainer("Misty");
	addTrainer("brock");
	console.log(theTrainer);

	// reading from Arrays 
/*
	-Accessing array Elements is one of the more common task that we do w/ an array
	-this can be done through the use of array indexes
	-each element is an array is associated w/ its own index/number

	-the reason an array starts w/ 0 is due to how the language is designaed

	syntax
	arrayName[index]

*/

console.log(grades[0]);
console.log(computerBrands[3]);

console.log(computerBrands[20]);//undifined

let lakersLegends = ['kobe','shaq','LeBron','Magic','Kareem'];

console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

let currentLaker =lakersLegends[2];
console.log(currentLaker)


console.log(lakersLegends)
lakersLegends[2]='Pau Gasol'
console.log(lakersLegends)

//  receive index number
function findBlackMamba(index) {
		return lakersLegends[index]
}
let blackMamba =findBlackMamba(0);
console.log(blackMamba);


// update index
let favoriteFoods = [ 'tonkatsu', 'Adobo', 'Hamburger', 'sinigang', 'pizza'];
favoriteFoods[3] = 'Kinilaw';
favoriteFoods[4] = 'tuba';
console.log(favoriteFoods)




// accessing the last element of an array
let bullsLegends =['jordan', 'Pippen','Rodman', 'Rose', 'kukoc'];

let lastElementIndex = bullsLegends.length - 1 ;
console.log(lastElementIndex);//4
console.log(bullsLegends.length);//5

console.log(bullsLegends[lastElementIndex]);
console.log(bullsLegends[bullsLegends.length - 1]);


// adding item into array
let newArr = [];
console.log(newArr[0]);
newArr[0]="cloud strife";
console.log(newArr)

console.log(newArr[1]);
newArr[1]="tifa lockhart";
console.log(newArr)

newArr[newArr.length-1]="Aerith Gains";
newArr[newArr.length]="bareet";


//looping over an arry
for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index])
}

/**/
let numArr = [ 5,12,30,46,40]

for(let index =0 ; index<numArr.length; index++){
	if(numArr[index] % 5==0){
		console.log(numArr[index] + ' is divisible by 5')
	}else{
		console.log(numArr[index] + ' is not divisible by 5')
	}
}


// multidimensional arrays
/*
	-multidimensional arrays are usefull for storing complex data structures
	-a practical applicatoin of this is to help visualiza/create real world object
*/

let chessBoard = [

	['a1','b1','c1','d1','e1','f1','g1','h1'],
	['a2','b2','c2','d2','e2','f2','g2','h2'],
	['a3','b3','c3','d3','e3','f3','g3','h3'],
	['a4','b4','c4','d4','e4','f4','g4','h4'],
	['a5','b5','c5','d5','e5','f5','g5','h5'],
	['a6','b6','c6','d6','e6','f6','g6','h6'],
	['a7','b7','c7','d7','e7','f7','g7','h7'],
	['a8','b8','c8','d8','e8','f8','g8','h8']
];
console.log(chessBoard);

console.log(chessBoard[1][4]);
console.log(chessBoard[7][0]);
console.log(chessBoard[5][7]);